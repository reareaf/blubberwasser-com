<?php
/*
Plugin Name: CookieCuttr v2
Plugin URI: http://pluginhero.com/portfolio/cookiecuttr
Description: Enables CookieCuttr functionality to help achieve compliance with EU Cookie Legislation
Version: 2.0.2
Author: PluginHero
Author URI: http://pluginhero.com
License: 
*/

global $ph_cookiecuttr_version;
$ph_cookiecuttr_version = "2.0.2";

// configure default options and perform any other installation (or upgrade) activities
function pluginhero_cookiecuttr_install()
{
    global $ph_cookiecuttr_version;
    
    $currentVersion = get_option("ph_cookiecuttr_version");

    // default values
    $defaultOptions = array
    (
        "hidefeature" => "false",        
        "hidedeclineonly" => "false",
        "analytics" => "true",
        "declinebutton" => "false",
        "resetbutton" => "false",
        "overlayenabled" => "false",
        "policylink" => "/privacy-policy/",
        "cookiemessage" => __("We use cookies on this website, you can <a href=\"{{cookiePolicyLink}}\" title=\"read about our cookies\">read about them here</a>. To use the website as intended please..."),
        "analyticsmessage" => __("We use cookies, just to track visits to our website, we store no personal details. To use the website as intended please..."),
        "errormessage" => __("We\'re sorry, this feature places cookies in your browser and has been disabled. <br>To continue using this functionality, please"),
        "whataretheylink" => "http://www.allaboutcookies.org/",
        "disable" => "",
        "useuniversalanalytics" => "false",
        "googleanalyticsid" => "",
        "showanalyticsuntildeclined" => "false",
        "showanalyticsonadminpages" => "false",
        "footerjs" => "false",
        "includejquerywp" => "false",
        "acceptcookiesmessage" => __("Accept Cookies"),
        "declinecookiesmessage" => __("Decline Cookies"),
        "resetcookiesmessage" => __("Reset Cookies"),
        "whatarecookiesmessage" => __("What are Cookies?"),
        "notificationlocationbottom" => "false",
        "cookieacceptbutton" => "false",
        "policypagemessage" => __("Please read the information below and then choose from the following options"),
        "discreetlink" => "false",
        "discreetreset" => "false",    
        "discreetlinktext" => __("Cookies?"),    
        "discreetlinkposition" => "topright",   
        "nomessage" => "false",    
        "domain" => "",         
    );
    
    // check for legacy version (1.x.x) or new install
    if(!$currentVersion || strlen($currentVersion) == 0)
    {
        if(get_option("cookiecuttr_version") && strlen(get_option("cookiecuttr_version")) > 0)
        {
            // legacy coversion required - update defaults
            $defaultOptions = array
            (
                "hidefeature" => get_option("cookiecuttr_hidefeature"),        
                "hidedeclineonly" => get_option("cookiecuttr_hidedeclineonly"),
                "analytics" => get_option("cookiecuttr_analytics"),
                "declinebutton" => get_option("cookiecuttr_declinebutton"),
                "resetbutton" => get_option("cookiecuttr_resetbutton"),
                "overlayenabled" => get_option("cookiecuttr_overlayenabled"),
                "policylink" => get_option("cookiecuttr_policylink"),
                "cookiemessage" => get_option("cookiecuttr_cookiemessage"),
                "analyticsmessage" => get_option("cookiecuttr_analyticsmessage"),
                "errormessage" => get_option("cookiecuttr_errormessage"),
                "whataretheylink" => get_option("cookiecuttr_whataretheylink"),
                "disable" => get_option("cookiecuttr_disable"),
                "useuniversalanalytics" => "false",
                "googleanalyticsid" => get_option("cookiecuttr_googleanalyticsid"),
                "showanalyticsuntildeclined" => get_option("cookiecuttr_showanalyticsuntildeclined"),
                "footerjs" => get_option("cookiecuttr_footerjs"),
                "includejquerywp" => get_option("cookiecuttr_includejquerywp"),
                "acceptcookiesmessage" => get_option("cookiecuttr_acceptcookiesmessage"),
                "declinecookiesmessage" => get_option("cookiecuttr_declinecookiesmessage"),
                "resetcookiesmessage" => get_option("cookiecuttr_resetcookiesmessage"),
                "whatarecookiesmessage" => get_option("cookiecuttr_whatarecookiesmessage"),
                "notificationlocationbottom" => get_option("cookiecuttr_notificationlocationbottom"),
                "cookieacceptbutton" => get_option("cookiecuttr_cookieacceptbutton"),
                "policypagemessage" => get_option("cookiecuttr_policypagemessage"),
                "discreetlink" => get_option("cookiecuttr_discreetlink"),
                "discreetreset" => get_option("cookiecuttr_discreetreset"),
                "discreetlinktext" => get_option("cookiecuttr_discreetlinktext"),
                "discreetlinkposition" => get_option("cookiecuttr_discreetlinkposition"),
                "nomessage" => get_option("cookiecuttr_nomessage"),
                "domain" => get_option("cookiecuttr_domain"),
            );
            
            // and delete old options
            delete_option("cookiecuttr_version", $ph_cookiecuttr_version);
            delete_option("cookiecuttr_hidefeature");
            delete_option("cookiecuttr_hidedeclineonly");
            delete_option("cookiecuttr_analytics");
            delete_option("cookiecuttr_declinebutton");
            delete_option("cookiecuttr_resetbutton");
            delete_option("cookiecuttr_overlayenabled");
            delete_option("cookiecuttr_policylink");
            delete_option("cookiecuttr_cookiemessage");
            delete_option("cookiecuttr_analyticsmessage");
            delete_option("cookiecuttr_errormessage");
            delete_option("cookiecuttr_whataretheylink");    
            delete_option("cookiecuttr_disable");    
            delete_option("cookiecuttr_googleanalyticsid");
            delete_option("cookiecuttr_showanalyticsuntildeclined");
            delete_option("cookiecuttr_footerjs");
            delete_option("cookiecuttr_includejquerywp");
            delete_option("cookiecuttr_acceptcookiesmessage");
            delete_option("cookiecuttr_declinecookiesmessage");
            delete_option("cookiecuttr_resetcookiesmessage");
            delete_option("cookiecuttr_whatarecookiesmessage");
            delete_option("cookiecuttr_notificationlocationbottom");
            delete_option("cookiecuttr_cookieacceptbutton");
            delete_option("cookiecuttr_policypagemessage");
            delete_option("cookiecuttr_discreetlink");    
            delete_option("cookiecuttr_discreetreset");    
            delete_option("cookiecuttr_discreetlinktext");    
            delete_option("cookiecuttr_discreetlinkposition");   
            delete_option("cookiecuttr_nomessage");    
            delete_option("cookiecuttr_domain"); 
        }
        
        update_option("ph_cookiecuttr", $defaultOptions);
    } 
    
    // upgrade from older version of 2.x
    if($currentVersion != $ph_cookiecuttr_version)
    {     
        // do update here if required (switch on current version before we change it)
        if($currentVersion == "2.0.0")
        {
            // try to delete old PHP file to avoid confusion (housekeeping)
            try 
            {
                unlink(plugin_dir_path( __FILE__ ) . '/wp.cookiecuttr.php');
            } 
            catch (Exception $e) 
            {
            }
        }

        // add new setting to 2.0.0+
        if($currentVersion == "2.0.0" || $currentVersion == "2.0.1")
        {
            $options = get_option("ph_cookiecuttr");
            $options["showanalyticsonadminpages"] = "false";
            update_option("ph_cookiecuttr", $options);
        }
        
        // set version number
        update_option("ph_cookiecuttr_version", $ph_cookiecuttr_version);
    }
}

// Called during update check - compare versions and re-run installer if required
function pluginhero_cookiecuttr_update_check() 
{
    global $ph_cookiecuttr_version;
    
    if (get_site_option("ph_cookiecuttr_version") != $ph_cookiecuttr_version) 
    {
        pluginhero_cookiecuttr_install();
    }
    
    // also load language files
    $pluginDir = basename(dirname(__FILE__));
    load_plugin_textdomain( 'ph_cookiecuttr', false, $pluginDir );        
}

// Add settings menu to WordPress
function pluginhero_cookiecuttr_plugin_menu() 
{
    add_options_page(__('CookieCuttr'), __('CookieCuttr'), 'manage_options', 'cookiecuttr-plugin-menu', 'pluginhero_cookiecuttr_plugin_options');
}

// Output custom options/settings/config page
function pluginhero_cookiecuttr_plugin_options() 
{
    if (!current_user_can('manage_options'))  
    {
        wp_die( __('You do not have sufficient permissions to access this page.') );
    }

    // if postback, update settings using update_option here
    if(isset($_POST["is_postback"]))
    {
        $updatedOptions = array
        (
            "hidefeature" => isset($_POST["cookiecuttr_hidefeature"]) && $_POST["cookiecuttr_hidefeature"]=="true"?"true":"false",        
            "hidedeclineonly" => isset($_POST["cookiecuttr_hidedeclineonly"]) && $_POST["cookiecuttr_hidedeclineonly"]=="true"?"true":"false",
            "analytics" => $_POST["cookiecuttr_analytics"]=="true"?"true":"false",
            "declinebutton" => isset($_POST["cookiecuttr_declinebutton"]) && $_POST["cookiecuttr_declinebutton"]=="true"?"true":"false",
            "cookieacceptbutton" => isset($_POST["cookiecuttr_cookieacceptbutton"]) && $_POST["cookiecuttr_cookieacceptbutton"]=="true"?"true":"false",
            "resetbutton" => isset($_POST["cookiecuttr_resetbutton"]) && $_POST["cookiecuttr_resetbutton"]=="true"?"true":"false",
            "overlayenabled" => isset($_POST["cookiecuttr_overlayenabled"]) && $_POST["cookiecuttr_overlayenabled"]=="true"?"true":"false",
            "policylink" => $_POST["cookiecuttr_policylink"],
            "cookiemessage" => $_POST["cookiecuttr_cookiemessage"],
            "analyticsmessage" => $_POST["cookiecuttr_analyticsmessage"],
            "errormessage" => $_POST["cookiecuttr_errormessage"],
            "whataretheylink" => $_POST["cookiecuttr_whataretheylink"],
            "disable" => $_POST["cookiecuttr_disablemessage"],
            "useuniversalanalytics" => isset($_POST["cookiecuttr_useuniversalanalytics"]) && $_POST["cookiecuttr_useuniversalanalytics"]=="true"?"true":"false",
            "googleanalyticsid" => $_POST["cookiecuttr_googleanalyticsid"],
            "showanalyticsuntildeclined" => isset($_POST["cookiecuttr_showanalyticsuntildeclined"]) && $_POST["cookiecuttr_showanalyticsuntildeclined"]=="true"?"true":"false",
            "showanalyticsonadminpages" => isset($_POST["cookiecuttr_showanalyticsonadminpages"]) && $_POST["cookiecuttr_showanalyticsonadminpages"]=="true"?"true":"false",
            "footerjs" => isset($_POST["cookiecuttr_footerjs"]) && $_POST["cookiecuttr_footerjs"]=="true"?"true":"false",
            "includejquerywp" => isset($_POST["cookiecuttr_includejquerywp"]) && $_POST["cookiecuttr_includejquerywp"]=="true"?"true":"false",
            "acceptcookiesmessage" => $_POST["cookiecuttr_acceptcookiesmessage"],
            "declinecookiesmessage" => $_POST["cookiecuttr_declinecookiesmessage"],
            "resetcookiesmessage" => $_POST["cookiecuttr_resetcookiesmessage"],
            "whatarecookiesmessage" => $_POST["cookiecuttr_whatarecookiesmessage"],
            "notificationlocationbottom" => isset($_POST["cookiecuttr_notificationlocationbottom"]) && $_POST["cookiecuttr_notificationlocationbottom"]=="true"?"true":"false",
            "policypagemessage" => $_POST["cookiecuttr_policypagemessage"],
            "discreetlink" => isset($_POST["cookiecuttr_discreetlink"]) && $_POST["cookiecuttr_discreetlink"]=="true"?"true":"false",
            "discreetreset" => isset($_POST["cookiecuttr_discreetreset"]) && $_POST["cookiecuttr_discreetreset"]=="true"?"true":"false",
            "discreetlinktext" => $_POST["cookiecuttr_discreetlinktext"],
            "discreetlinkposition" => $_POST["cookiecuttr_discreetlinkposition"],
            "nomessage" => isset($_POST["cookiecuttr_nomessage"]) && $_POST["cookiecuttr_nomessage"]=="true"?"true":"false",
            "domain" => $_POST["cookiecuttr_domain"]
        );
        
        update_option("ph_cookiecuttr", $updatedOptions);
        
        ?>
            <div class="updated"><p><strong><?php _e('settings saved.', 'menu-test' ); ?></strong></p></div>
        <?php
    }
    
    $path = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
    $options = get_option("ph_cookiecuttr");
    
    ?>
    <div class="wrap cookiecuttr">
        <div id="icon-options-general" class="icon32"></div>
        <h2><?php _e("CookieCuttr Options"); ?></h2>
        <p>
            <?php _e("You can edit all of the various options for CookieCuttr below, you will then need to wrap the following code around the script block...</p>"); ?>
        <p>
            if($.cookieCuttr.accepted) { <br />
            &nbsp;&nbsp;&nbsp;// <?php _e("this code will not run until cookies have been accepted"); ?> <br />
            } <br /> <br />

            if($.cookieCuttr.declined) { <br />
            &nbsp;&nbsp;&nbsp;// <?php _e("this code will not run until cookies have been declined"); ?> <br />
            }
        </p>
        <p>
            <?php _e("To activate a different message on your policy page, allowing the website visitor to opt in/out just copy and paste the following shortcode anywhere in your pages content."); ?>
            <br /><br />
            [cookiecuttrprivacy]
        </p> 
    </div>
    <form name="form1" method="post" action="">
        <input type="hidden" name="is_postback" value="1" />

             <div class="cookiecuttr-col">
             <h2 class="cc-top"><?php _e("Button &amp; Link Text"); ?></h2>
             <ul>
             <li>
                 <label for="cookiecuttr_acceptcookiesmessage"><?php _e("Change 'Accept Cookies' button text"); ?></label><br />
                 <input class="cookiecuttr-input" type="text" id="cookiecuttr_acceptcookiesmessage" name="cookiecuttr_acceptcookiesmessage" value="<?php echo($options["acceptcookiesmessage"]);?>" />
             </li>
             <li>
                 <label for="cookiecuttr_declinecookiesmessage"><?php _e("Change 'Decline Cookies' button text"); ?></label><br />
                 <input class="cookiecuttr-input" type="text" id="cookiecuttr_declinecookiesmessage" name="cookiecuttr_declinecookiesmessage" value="<?php echo($options["declinecookiesmessage"]);?>" />
             </li>
             <li>
                 <label for="cookiecuttr_resetcookiesmessage"><?php _e("Change 'Reset Cookies' button text"); ?></label><br />
                 <input class="cookiecuttr-input" type="text" id="cookiecuttr_resetcookiesmessage" name="cookiecuttr_resetcookiesmessage" value="<?php echo($options["resetcookiesmessage"]);?>" />
             </li>
             <li>
                 <label for="cookiecuttr_whatarecookiesmessage"><?php _e("Change 'What are Cookies' link text"); ?></label><br />
                 <input class="cookiecuttr-input" type="text" id="cookiecuttr_whatarecookiesmessage" name="cookiecuttr_whatarecookiesmessage" value="<?php echo($options["whatarecookiesmessage"]);?>" />
             </li>
             <li>
                 <label for="cookiecuttr_discreetlinktext"><?php _e("Change 'Discreet Link' text"); ?></label><br />
                 <input class="cookiecuttr-input" type="text" id="cookiecuttr_policypagemessage" name="cookiecuttr_discreetlinktext" value="<?php echo(stripslashes($options["discreetlinktext"]));?>" />
             </li>   
             </ul>
             <h2><?php _e("Messages"); ?></h2>
            <ul>
            <li>
                <label for="cookiecuttr_analyticsmessage"><?php _e("Analytics message (this is the default message)"); ?></label><br />
                <textarea class="cookiecuttr-textarea" id="cookiecuttr_analyticsmessage" name="cookiecuttr_analyticsmessage"><?php echo(stripslashes($options["analyticsmessage"]));?></textarea>
            </li>
            <li>
                <label for="cookiecuttr_cookiemessage"><?php _e("'Cookie Privacy link' Message (used when Analytics tick box is off)"); ?></label><br />
                <textarea class="cookiecuttr-textarea" id="cookiecuttr_cookiemessage" name="cookiecuttr_cookiemessage"><?php echo(stripslashes($options["cookiemessage"]));?></textarea>
                <br /><br />
                <span class="cookiecuttr-tooltip"><?php _e("Please keep the {{cookiePolicyLink}} in tact for this message to work properly"); ?></span>
                <br />
            </li>
            <li>
                <label for="cookiecuttr_errormessage"><?php _e("'Error' message (used when Enable div etc. hide feature is on)"); ?></label><br />
                <textarea class="cookiecuttr-textarea" id="cookiecuttr_errormessage" name="cookiecuttr_errormessage"><?php echo(stripslashes($options["errormessage"]));?></textarea>
            </li>
            <li>
                <label for="cookiecuttr_policypagemessage"><?php _e("'Privacy Policy Page' message"); ?></label><br />
                <input class="cookiecuttr-input" type="text" id="cookiecuttr_policypagemessage" name="cookiecuttr_policypagemessage" value="<?php echo(stripslashes($options["policypagemessage"]));?>" />
            </li>
            </ul>

            <h2><?php _e("Links"); ?></h2>
            <ul>
            <li>
                <label for="cookiecuttr_policylink"><?php _e("Your Privacy policy / Cookie policy link"); ?></label><br />
                <input class="cookiecuttr-input" type="text" id="cookiecuttr_policylink" name="cookiecuttr_policylink" value="<?php echo($options["policylink"]);?>" />
            </li>
            <li>
                <label for="cookiecuttr_whataretheylink"><?php _e("'What are Cookies' link"); ?></label><br />
                <input class="cookiecuttr-input" type="text" id="cookiecuttr_whataretheylink" name="cookiecuttr_whataretheylink" value="<?php echo($options["whataretheylink"]);?>" />
            </li>
            </ul>
            <h2><?php _e("Cookie Bar / Discreet Link Positioning"); ?></h2>   
            <ul>
            <li>
                <label for="cookiecuttr_notificationlocationbottom"><?php _e("Display the notification at the bottom of the page?"); ?></label>
                <input type="checkbox" id="cookiecuttr_notificationlocationbottom" name="cookiecuttr_notificationlocationbottom" <?php echo($options["notificationlocationbottom"]=="true"?"checked":""); ?> value="true"/><br /><br />
            </li>              
            <li>
                <label for="cookiecuttr_discreetlinkposition"><?php _e("'Discreet' position (This positions Discreet Link and Discreet Reset)"); ?></label><br /><br />
                <select id="cookiecuttr_discreetlinkposition" name="cookiecuttr_discreetlinkposition">
                    <option value="topright" <?php echo($options["discreetlinkposition"] == "topright"?" selected":""); ?>>Top Right</option>
                    <option value="topleft" <?php echo($options["discreetlinkposition"] == "topleft"?" selected":""); ?>>Top Left</option>
                    <option value="bottomright" <?php echo($options["discreetlinkposition"] == "bottomright"?" selected":""); ?>>Bottom Right</option>
                    <option value="bottomleft" <?php echo($options["discreetlinkposition"] == "bottomleft"?" selected":""); ?>>Bottom Left</option>
                </select>
            </li>                    
            </ul>
            <h2><?php _e("Disable Page elements"); ?></h2>
            <ul>
                    <li>
                    <label for="cookiecuttr_hidefeature"><?php _e("Enable the page element disabler"); ?></label>
                    <input type="checkbox" id="cookiecuttr_hidefeature" name="cookiecuttr_hidefeature" <?php echo($options["hidefeature"]=="true"?"checked":""); ?> value="true"/>
                    <br /><br />
                </li>
                <li>
                    <label for="cookiecuttr_hidedeclineonly"><?php _e("Only activate page element disabler when visitor Declines cookies (implied consent)"); ?></label>
                    <input type="checkbox" id="cookiecuttr_hidedeclineonly" name="cookiecuttr_hidedeclineonly" <?php echo($options["hidedeclineonly"]=="true"?"checked":""); ?> value="true"/>
                    <br /><br />
                    <span class="cookiecuttr-tooltip"><?php _e('For this option to work, "Enable the page element disabler" must be ticked'); ?></span>
                    <br />
                </li>
                <li>
                    <label for="cookiecuttr_disablemessage"><?php _e("Enter the CSS Selectors (comma separated) that you want to disable e.g. .comments, #footer, span.advert, section.advert"); ?></label><br />
                    <input class="cookiecuttr-input" id="cookiecuttr_disablemessage" name="cookiecuttr_disablemessage" type="text" value="<?php echo(stripslashes($options["disable"]));?>" />
                </li>
            </ul>    
        </div>

        <div class="cookiecuttr-col">
        <h2 class="cc-top"><?php _e("Google Analytics"); ?></h2>
        <ul>
        <li>
            <label for="cookiecuttr_useuniversalanalytics"><?php _e("Use 'Universal Analytics' tracking code?"); ?></label>
            <input type="checkbox" id="cookiecuttr_useuniversalanalytics" name="cookiecuttr_useuniversalanalytics" <?php echo($options["useuniversalanalytics"]=="true"?"checked":""); ?> value="true"/>
            <br /><br />
            <span class="cookiecuttr-tooltip"><?php _e("'Universal Analytics' is the new version of Google Analytics. If you've converted your account and want to use the new tracking code, enable this option."); ?> <a href="https://support.google.com/analytics/answer/2790010?hl=en" target="_blank">https://support.google.com/analytics/answer/2790010?hl=en</a></span><br />            
        </li>
        <li>
            <label for="cookiecuttr_googleanalyticsid"><?php _e("Enter your Google Analytics ID"); ?></label>
            <input class="cookiecuttr-input" type="text" id="cookiecuttr_googleanalyticsid" name="cookiecuttr_googleanalyticsid" value="<?php echo $options["googleanalyticsid"];?>" />
            <br /><br />
            <span class="cookiecuttr-tooltip"><?php _e("You need to disable any other plugins that are currently running your Google Analytics and remove any Google Analytics code you have in your templates for this option to work."); ?></span><br />
        </li>
        <li>
            <label for="cookiecuttr_showanalyticsuntildeclined"><?php _e("Enable Google Analytics until the website visitor has declined cookies? (Implied consent)"); ?></label>
            <input type="checkbox" id="cookiecuttr_showanalyticsuntildeclined" name="cookiecuttr_showanalyticsuntildeclined" <?php echo($options["showanalyticsuntildeclined"]=="true"?"checked":""); ?> value="true"/><br /><br />
        </li>  
        <li>
            <label for="cookiecuttr_showanalyticsonadminpages"><?php _e("Enable Google Analytics on 'Admin' pages?"); ?></label>
            <input type="checkbox" id="cookiecuttr_showanalyticsonadminpages" name="cookiecuttr_showanalyticsonadminpages" <?php echo($options["showanalyticsonadminpages"]=="true"?"checked":""); ?> value="true"/><br /><br />
        </li>  
        <li>
            <label for="cookiecuttr_domain"><?php _e("Enter your domain name (no http://www.)"); ?></label>
            <input class="cookiecuttr-input" type="text" id="cookiecuttr_domain" name="cookiecuttr_domain" value="<?php echo $options["domain"];?>" />
            <br /><br />
            <span class="cookiecuttr-tooltip"><?php _e("If you'd like us to remove Google Analytics cookies when a user declines cookies, enter your domain name in above with no www. e.g. if your domain name is http://www.cookiecuttr.com please enter in cookiecuttr.com"); ?></span><br />
        </li>                  
        </ul>

        <h2><?php _e("Disable all cookie notification bars"); ?></h2>
        <ul>
        <li>
            <label for="cookiecuttr_nomessage"><?php _e("Yes please"); ?></label>
            <input type="checkbox" id="cookiecuttr_nomessage" name="cookiecuttr_nomessage" <?php echo($options["nomessage"]=="true"?"checked":""); ?> value="true"/>
            <br /><br />
            <span class="cookiecuttr-tooltip"><?php _e("This option should be used in conjunction with adding the shortcode [cookiecuttrprivacy] to your policy page as detailed above. You must make your policy page clear and obvious in your menu/template. This option overrides all the below options. This does not hide the 'Privacy Policy Page Only' message."); ?></span><br />
        </li>
        </ul>

        <h2><?php _e("Show these buttons..."); ?></h2>
        <ul class="cookiecuttr-three">
            <li>
                <label for="cookiecuttr_cookieacceptbutton"><?php _e("Accept"); ?></label>
                <input type="checkbox" id="cookiecuttr_cookieacceptbutton" name="cookiecuttr_cookieacceptbutton" <?php echo($options["cookieacceptbutton"]=="true"?"checked":""); ?> value="true"/>
            </li>
            <li>
                <label for="cookiecuttr_declinebutton"><?php _e("Decline"); ?></label>
                <input type="checkbox" id="cookiecuttr_declinebutton" name="cookiecuttr_declinebutton" <?php echo($options["declinebutton"]=="true"?"checked":""); ?> value="true"/>
            </li>
            <li>
                <label for="cookiecuttr_resetbutton"><?php _e("Reset"); ?></label>
                <input type="checkbox" id="cookiecuttr_resetbutton" name="cookiecuttr_resetbutton" <?php echo($options["resetbutton"]=="true"?"checked":""); ?> value="true"/>
            </li>
        </ul>

        <h2><?php _e("Cookie Bar style"); ?></h2>
        <ul>
        <li>
            <label for="cookiecuttr_discreetlink"><?php _e('Display the "Discreet Link"?'); ?></label>
                            <input type="checkbox" id="cookiecuttr_discreetlink" name="cookiecuttr_discreetlink" <?php echo($options["discreetlink"]=="true"?"checked":""); ?> value="true"/><br /><br />
        <span class="cookiecuttr-tooltip"><?php _e("Selecting this option will automatically switch off the other messages on the website and replace with your discreet link text and link, this option should be used in conjunction with adding the shortcode [cookiecuttrprivacy] to your policy page as detailed above."); ?></span><br />
        </li>
        <li>
            <label for="cookiecuttr_discreetreset"><?php _e("Make the reset button discreet ?"); ?></label>
            <input type="checkbox" id="cookiecuttr_discreetreset" name="cookiecuttr_discreetreset" <?php echo($options["discreetreset"]=="true"?"checked":""); ?> value="true"/>
            <br /><br />
        </li>
        <li>
            <label for="cookiecuttr_overlayenabled"><?php _e("Don't want a discreet toolbar? Give me a big overlay!"); ?></label>
            <input type="checkbox" id="cookiecuttr_overlayenabled" name="cookiecuttr_overlayenabled" <?php echo($options["overlayenabled"]=="true"?"checked":""); ?> value="true"/>
        </li>
        </ul>
        <h2><?php _e("Toggle Messages"); ?></h2>
            <ul>
            <li>
                <label for="cookiecuttr_analytics"><?php _e("Switch on for 'Analytics' message and off for 'Cookie Privacy Link' message"); ?></label>
                <input type="checkbox" id="cookiecuttr_analytics" name="cookiecuttr_analytics" <?php echo($options["analytics"]=="true"?"checked":""); ?> value="true"/>
            </li>
            </ul>

            <h2><?php _e("Technical implementation"); ?></h2>
            <ul>
            <li>
                <label for="cookiecuttr_footerjs"><?php _e("Output JavaScript in footer?"); ?></label>
                <input type="checkbox" id="cookiecuttr_footerjs" name="cookiecuttr_footerjs" <?php echo($options["footerjs"]=="true"?"checked":""); ?> value="true"/><br /><br />
            </li>                    
            <li>
                <label for="cookiecuttr_includejquerywp"><?php _e("Include jQuery via WordPress?"); ?></label>
                <input type="checkbox" id="cookiecuttr_includejquerywp" name="cookiecuttr_includejquerywp" <?php echo($options["includejquerywp"]=="true"?"checked":""); ?> value="true"/><br /><br />
                <span class="cookiecuttr-tooltip"><?php _e("The above 2 options can be checked if the cookie bar is not displaying or you are getting JavaScript errors."); ?></span>
                <br />
            </li>                    
         </ul>
        </div>
        <p class="cookiecuttr-submit">
                <input type="submit" name="Submit" class="cookiecuttr-button" value="<?php _e('Save Changes'); ?>" />
        </p>
    </form>
    <?php
}

// Load Scripts (and localise with configuration where appropriate)
function pluginhero_cookiecuttr_load_scripts()
{
    global $ph_cookiecuttr_version;

    $path = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
    $options = get_option("ph_cookiecuttr");

    // CSS
    wp_register_style("cookiecuttr_main", $path."cookiecuttr.css", false, false);
    wp_enqueue_style("cookiecuttr_main");

    // JS
    wp_deregister_script("cookiecuttr_js");
    
    if($options["includejquerywp"] == "true")
    {
        wp_register_script("cookiecuttr_js", $path . "jquery.cookiecuttr.js", array('jquery'), $ph_cookiecuttr_version);
        wp_enqueue_script("cookiecuttr_js", $path . "jquery.cookiecuttr.js", array("jquery"), $ph_cookiecuttr_version, $options["footerjs"] == "true");
    }
    else
    {
        wp_register_script("cookiecuttr_js", $path . "jquery.cookiecuttr.js", "", $ph_cookiecuttr_version);
        wp_enqueue_script("cookiecuttr_js", $path . "jquery.cookiecuttr.js", "", $ph_cookiecuttr_version, $options["footerjs"] == "true");
    }

    wp_localize_script("cookiecuttr_js", 'defaults', 
        array
        ( 
            'cookieCutter' => ($options["hidefeature"] == "true"?true:false),
            'cookieCutterDeclineOnly' => ($options["hidedeclineonly"] == "true"?true:false),
            'cookieAnalytics' => ($options["analytics"] == "true"?true:false),
            'cookieAcceptButton' => ($options["cookieacceptbutton"] == "true"?true:false),
            'cookieDeclineButton' => ($options["declinebutton"] == "true"?true:false),
            'cookieResetButton' => ($options["resetbutton"] == "true"?true:false),
            'cookieOverlayEnabled' => ($options["overlayenabled"] == "true"?true:false),
            'cookiePolicyLink' => $options["policylink"],
            'cookieMessage' => stripslashes($options["cookiemessage"]),
            'cookieAnalyticsMessage' => stripslashes($options["analyticsmessage"]),
            'cookieErrorMessage' => stripslashes($options["errormessage"]),
            'cookieWhatAreTheyLink' => $options["whataretheylink"],
            'cookieDisable' => $options["disable"],
            'cookieAnalyticsId' => $options["googleanalyticsid"],
            'cookieAcceptButtonText' => $options["acceptcookiesmessage"],
            'cookieDeclineButtonText' => $options["declinecookiesmessage"],
            'cookieResetButtonText' => $options["resetcookiesmessage"],
            'cookieWhatAreLinkText' => $options["whatarecookiesmessage"],
            'cookieNotificationLocationBottom' => ($options["notificationlocationbottom"] == "true"?true:false),
            'cookiePolicyPageMessage' => stripslashes($options["policypagemessage"]),
            'cookieDiscreetLink' => ($options["discreetlink"] == "true"?true:false),
            'cookieDiscreetReset' => ($options["discreetreset"] == "true"?true:false),            
            'cookieDiscreetLinkText' => stripslashes($options["discreetlinktext"]),
            'cookieDiscreetPosition' => $options["discreetlinkposition"],
            'cookieNoMessage' => ($options["nomessage"] == "true"?true:false),
            'cookieDomain' => $options["domain"]
        )
    );
    
    pluginhero_cookiecuttr_output_analytics();
}  

// output Google Analytics tracker (based on configuration)
function pluginhero_cookiecuttr_output_analytics()
{
    $options = get_option("ph_cookiecuttr");

    $googleAnalyticsId = $options["googleanalyticsid"];
    
    if(strlen($googleAnalyticsId) > 0)
    {
        $cookieAccept = isset($_COOKIE["cc_cookie_accept"]) && $_COOKIE["cc_cookie_accept"] == "cc_cookie_accept";
        $cookieDecline = isset($_COOKIE["cc_cookie_decline"]) && $_COOKIE["cc_cookie_decline"] == "cc_cookie_decline";
        
        if($cookieAccept || ($options["showanalyticsuntildeclined"] == "true" && !$cookieDecline))
        {
            if($options["useuniversalanalytics"]=="false")
            {
                // Classic Analytics
                ?>
<script language="javascript" type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?php echo($googleAnalyticsId); ?>']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
                <?php
            }
            else
            {
                // Universal Analytics
                ?>
<script language="javascript" type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', '<?php echo($googleAnalyticsId); ?>', 'auto');
ga('send', 'pageview');

</script>
                <?php
            }
        }
    }
}

// Called by WordPress framework - output the admin CSS
function pluginhero_cookiecuttr_admin_styles()
{
    // admin css
    wp_register_style( 'ph-cookiecuttr-admin', plugins_url('admin.css', __FILE__) );    
    wp_enqueue_style("ph-cookiecuttr-admin");
    
    $options = get_option("ph_cookiecuttr");

    if(isset($options["showanalyticsonadminpages"]) && $options["showanalyticsonadminpages"] == "true")
    {
        pluginhero_cookiecuttr_output_analytics();
    }
}

// Output privacy page shortcode (see documentation for details)
function pluginhero_cookiecuttr_privacypage_shortcode($attributes)
{
    return "<input type='hidden' id='cookiecuttr_privacypage' value='1' />";
}

// hooks
register_activation_hook(__FILE__,'pluginhero_cookiecuttr_install');

// actions
add_action('plugins_loaded', 'pluginhero_cookiecuttr_update_check');
add_action('admin_menu', 'pluginhero_cookiecuttr_plugin_menu');
add_action('wp_enqueue_scripts', 'pluginhero_cookiecuttr_load_scripts', 50);
add_action('admin_enqueue_scripts', 'pluginhero_cookiecuttr_admin_styles');    

// shortcodes
add_shortcode( 'cookiecuttrprivacy','pluginhero_cookiecuttr_privacypage_shortcode');
?>