<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Paperback
 */

// Get layout style
$homepage_layout = get_option( 'paperback_layout_style', 'one-column' );

// Get the sidebar widgets
if ( is_active_sidebar( 'sidebar' ) ) {
	if ( $homepage_layout === 'one-column' || $homepage_layout === 'two-column' || is_single() || is_page() ) {
	?>
	<div id="secondary" class="widget-area">

	<?php if( get_field('listchildren_status') ): ?>
	<aside id="egm_list_children-2" class="widget widget_egm_list_children">
		<h2 class="widget-title"></h2>
		<a href="<?php echo get_field( "listchildren_parent_url" ); ?>" title="<?php echo get_field( "listchildren_parent_title" ); ?>" class="egm-list-parent">
			<span><?php echo get_field( "listchildren_parent_title" ); ?></span>
			<?php
			$imageParent = get_field('listchildren_parent_image');
			if( !empty($imageParent) ): ?>
				<img width="100" height="100" src="<?php echo $imageParent['url']; ?>" alt="<?php echo $imageParent['alt']; ?>" />
			<?php endif; ?>

		</a>
		<ul>


		<?php
		if( have_rows('listchildren_child') ):
		    while ( have_rows('listchildren_child') ) : the_row();

				// Image in Variable
				$imageChild = get_sub_field('listchildren_child_image');
		?>

		<a href="<?php the_sub_field('listchildren_child_url'); ?>" title="<?php the_sub_field('listchildren_child_title'); ?>" class="egm-list-child">
			<li>
			<?php
			if( !empty($imageChild) ): ?>
				<img width="35" height="35" class="attachment-affiliateSugarRelatedThumbnailSizeSmall size-affiliateSugarRelatedThumbnailSizeSmall wp-post-image" src="<?php echo $imageChild['url']; ?>" alt="<?php echo $imageChild['alt']; ?>" />
			<?php endif; ?>
				<span><?php the_sub_field('listchildren_child_title'); ?></span>
			</li>
		</a>

		<?php
		    endwhile;

		endif;
		?>

		</ul>
	</aside>

	<?php endif; ?>

		<?php do_action( 'paperback_above_sidebar' );

		dynamic_sidebar( 'sidebar' );

		do_action( 'paperback_below_sidebar' ); ?>
	</div><!-- #secondary .widget-area -->
<?php } } ?>
