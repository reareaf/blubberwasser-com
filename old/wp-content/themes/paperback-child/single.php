<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Paperback
 */

get_header();

$comment_style = get_option( 'paperback_comment_style', 'click' );

if ( comments_open() ) {
	$comments_status = 'open';
} else {
	$comments_status = 'closed';
}
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


			<div class="egm-brand egm-brand--full">
				<span class="egm-brand__title">
					<a href="<?php echo get_field( "brandbox_affiliate" ); ?>" target="_blank" rel="nofollow noopener"><?php echo get_field( "brandbox_brandname" ); ?></a>
				</span><span class="egm-brand__sub-title">
				<a href="<?php echo get_field( "brandbox_affiliate" ); ?>" target="_blank" rel="nofollow noopener"><?php echo get_field( "brandbox_subtitle" ); ?></a></span>
			<div class="egm-brand__image"><a class="no-underline" href="<?php echo get_field( "brandbox_affiliate" ); ?>" target="_blank" rel="nofollow noopener">
			<?php
				$imageLogo = get_field('brandbox_logo');
				if( !empty($imageLogo) ): ?>
					<img src="<?php echo $imageLogo['url']; ?>" alt="<?php echo $imageLogo['alt']; ?>" class="attachment-large size-large" data-was-processed="true" />
			<?php endif; ?>
			</a></div>
			<div class="egm-brand__content">

			<?php
			if (have_rows('brandbox_brand_content') {
			    while (have_rows('brandbox_brand_content') {
						$brandContentLink = get_field('brandbox_brand_content_url');

						if($brandContentLink) {	?>
						<strong>
							<?php the_sub_field('brandbox_brand_content_category'); ?>
						</strong>
						<a href="<?php echo $brandContentLink ?>" target="_blank" rel="nofollow noopener">
							<?php the_sub_field('brandbox_brand_content_value'); ?><br/>
						</a>

						<?php
						the_sub_field('brandbox_brand_content_value');
					}
			} else {
				the_row();
			}
		}	?>
			<a class="egm-brand__button" href="<?php echo get_field( "brandbox_affiliate" ); ?>" target="_blank" rel="nofollow noopener">Zum Shop</a></div>
			</div>

		<?php while ( have_posts() ) : the_post();

			// Move Jetpack share links below author box
			if ( function_exists( 'sharing_display' ) && ! function_exists( 'dsq_comment' ) ) {
				remove_filter( 'the_content', 'sharing_display', 19 );
				remove_filter( 'the_excerpt', 'sharing_display', 19 );
			}

			// Post content template
			get_template_part( 'template-parts/content' );

			// Author profile box
			paperback_author_box();

			// Related Posts
			if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
				echo do_shortcode( '[jetpack-related-posts]' );
			} ?>

			<!-- Comment toggle and share buttons -->
			<div class="share-comment <?php echo esc_attr( $comment_style ); ?>">

				<?php if ( function_exists( 'sharing_display' ) ) { ?>
					<div class="share-icons <?php echo esc_attr( $comments_status ); ?>">
						<?php echo sharing_display(); ?>
					</div>
				<?php } ?>

				<?php if ( comments_open() ) { ?>
					<a class="comments-toggle button" href="#">
						<span><i class="fa fa-comments"></i>
							<?php if ( '0' != get_comments_number() ) {
								esc_html_e( 'Show comments', 'paperback' );
							} else {
								esc_html_e( 'Leave a comment', 'paperback' );
							} ?>
						</span>
						<span><i class="fa fa-times"></i> <?php esc_html_e( 'Hide comments', 'paperback' ); ?></span>
					</a>
				<?php } ?>
			</div>

			<?php // Comments template
			comments_template();

		endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php get_sidebar();

get_footer(); ?>
