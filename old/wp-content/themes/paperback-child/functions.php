<?php
/*
 * Allgemeine Funktions Klasse des WP Themes paperback
 *
 * @author        Author <raffael@raffaelernst.ch>
 */




function child_theme_styles() {
wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
wp_enqueue_style( 'child-theme-css', get_stylesheet_directory_uri() .'/style.css' , array('parent-style'));

}
add_action( 'wp_enqueue_scripts', 'child_theme_styles' );






function brandbox_func( ) {

	$brandname = get_field( "brandbox_brandname" );

	return $brandname;
}
add_shortcode( 'brandbox', 'brandbox_func' );
















?>
